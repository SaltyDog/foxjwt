# FoxJWT

Class to encode/decode JWT files from VFP code. Uses wwDotNetBridge to call .Net classes that do the actual work.